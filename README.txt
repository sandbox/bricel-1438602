
MojoZoom
===============

The MojoZoom module is a plugin that can be used to output a zoom area of an
image using the gallery formatter module.

Installation
============
Download mojozoom library from http://www.nihilogic.dk/labs/mojozoom/ and place
into sites/all/library/mojozoom.

Download and install Gallery formater.


Requirements
============

Library and Gallery formater (with mojozoom patch) modules


Description
===========

This module will add an new modal full screem option.
Settings are available for image style of zoom file.


Authors/maintainers
===================

Original Author:

Brice lenfant
http://drupal.org/user/552488


Support
=======

Issues should be posted in the issue queue on drupal.org:
http://drupal.org/sandbox/bricel/1438602

