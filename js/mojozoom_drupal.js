(function ($) {
  Drupal.behaviors.mojozoom = {
    attach: function (context) {
      // Bind visibility event with show binder
      $.each(["show", "toggle", "toggleClass", "addClass", "removeClass"], function(){
        var _oldFn = $.fn[this];
        $.fn[this] = function(){
          var hidden = this.find(":hidden").add(this.filter(":hidden"));
          var result = _oldFn.apply(this, arguments);
          hidden.filter(":visible").each(function(){
            $(this).triggerHandler("show"); //No bubbling
          });
          return result;
        }
      });


      // image    = the image element you want to add the effect to
      // source   = the source of the high res version of the image
      // zoomElement = optional element to hold the zoomed portion. If not specified, a default 256x256 square is created.
      // zoomWidth    = optional width of zoomElement
      // zoomHeight   = optional height of zoomElement
      // alwaysShow   = if true, the high res image will always be displayed, regardless of mouseover/out events.
      $('.gallery-frame li').bind("show", function(){
          MojoZoom.makeZoomable(
            $(this).children('img').get(0),
            $(this).children('a').attr('href'),
            $('#mojo_zoom_area').get(0),
            250, 300,
            false
          );
      });
    }
  };
})(jQuery);
